from django.core import serializers
from django.forms import model_to_dict
from django.http import HttpResponse, JsonResponse
from firebase_admin import credentials
from django.views.decorators.csrf import csrf_exempt, csrf_protect
import firebase_admin
import firebase_admin.auth as auth
import json
from producer import generate_event

from user.models import User
from user.serializer import UserSerializer

cred = credentials.Certificate('/var/local/firebase_cred.json')
default_app = firebase_admin.initialize_app(cred)


def index(request):
    print(default_app.name)
    print(auth.list_users().users)
    return HttpResponse("Hello, world. You're at the polls index.")


@csrf_exempt
def register(request):
    if request.method != "POST":
        return HttpResponse(status=401)
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    email = body['email']
    name = body['name']
    try:
        user = User(name=name, email=email)
        user.save()
        generate_event(f'A new user registered: {user.name}')
        return HttpResponse(status=200)
    except Exception as e:
        print(e)
        return HttpResponse(status=501)


@csrf_exempt
def login(request):
    if request.method != "POST":
        return HttpResponse(status=401)
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    email = body['email']
    password = body['password']
    try:
        user = auth.create_user(email=email, password=password)
        uid = user.uid
        jwt_token = auth.create_custom_token(uid).decode('utf-8')
        user = User(name=name, email=email)
        user.save()
        serializer = UserSerializer(user)
        content = serializer.data
        content["jwt_token"] = jwt_token

        return JsonResponse(content, status=201)
    except Exception as e:
        print(e)
        return HttpResponse(status=501)


def checkAccessToken(token, email):
    x = auth.verify_id_token(token)
    print(x)
    print(x['email'] == email)
    if not x['email'] == email:
        raise Exception("not authorized")