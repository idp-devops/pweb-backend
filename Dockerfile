FROM python:3.8
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY req.txt /code/
COPY firebase_cred.json /var/local/
RUN pip3 install -r req.txt
COPY . /code/
CMD [ "./run.sh" ]