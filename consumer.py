import pika
import requests

print("Worker started")
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost')
)
channel = connection.channel()
channel.queue_declare(queue='queue')

print("worker connected")

def callback(ch, method, properites, body):
    print(body.decode())
    if '[new_follow]' in body.decode():
        r = requests.get('https://webhook.site/1a227b5f-b963-4f7b-9227-93833e0a95fe')
        print(r)
        print("\t\tEmail sent! \n\n\n")

channel.basic_consume(queue='queue', auto_ack=True, on_message_callback=callback)
print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()