### Path
Send register request

`/auth/register`

### Body

````
{
    "email": "blawwwbla@fgrkh.com",
    "password": "leoeoeoewwwo",
    "name": "erfhr"
}
````
---
### Path
Follow a certain user

`/user/follow`

### Body
````
{
    "followed_user":{
        "email": "tesct.tefst@gccmail.com"
    },
    "current_user":{
        "email": "gbgbfgbgf@gmai.com"
    }
}
````
---
### Path
Get the user's following list

`/user/get-following`
### Body
````
{
    "email":"gbgbfgbgf@gmai.com"
}
````
---
### Path
Get ALL news (not filtered for each user)

`news/get-news`
### Body
````
[
    {
        "title": "sdfe",
        "description": "rwfer",
        "language": "english",
        "url": "sfdvaerkh",
        "date": "ferg",
        "comments": [
            {
                "user": {
                    "name": "ffyhjf",
                    "email": "jty@ggmail.com",
                    "following": [],
                    "preferred_language": "english",
                    "location": "Kyiv"
                },
                "content": "Hello"
            }
        ]
    },
    {
        "title": "Small wins buoy Ukraine; West says Russians losing momentum",
        "description": "KYIV, Ukraine -- Almost three months after Russia shocked the world by invading Ukraine, its military faces a bogged-down war, the prospect of a bigger NATO and an opponent buoyed Sunday by wins on and off the battlefield. Top diplomats from NATO met in Berlin with the alliance's chief and declared that the war \"is not going as Moscow had planned.\" \"Ukraine can win this war,\" NATO Secretary-General Jens Stoltenberg said, adding that the alliance must continue to offer military support to Kyiv. He spoke by video link to the meeting as he recovers from a COVID-19 infection. On the",
        "language": "eng",
        "url": "https://www.denverpost.com/2022/05/15/russians-lose-momentum-ukraine-war/",
        "date": "2022-05-15T20:20:00Z",
        "comments": []
    }
````
---
### Path 
Add a comment to a certain article

`comments/add-comment`

### Body
````
{
    "email": "jty@ggmail.com",
    "url": "sfdvaerkh",
    "comment": "Hello"
}
````

:)
