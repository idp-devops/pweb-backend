from django.urls import path, re_path
from . import views

urlpatterns = [
    path('follow', views.follow_user),
    path('get-following/<str:email>', views.get_following_list),
    path('search/<str:email>/<str:search>', views.get_search_list),
    path('<str:email>', views.get_user),
    path('update/<str:email>', views.update_user)

]