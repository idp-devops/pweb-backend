import json

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from producer import generate_event
# Create your views here.
from user.models import User
from user.serializer import UserSerializer
from firebase_auth.views import checkAccessToken

@csrf_exempt
def update_user(request, email):
    if request.method != "POST":
        return HttpResponse(status=401)
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    current_user = User.objects.get(email=email)
    try:
        checkAccessToken(request.headers.get("Authorization"), email)
    except:
        return HttpResponse(status=401)
    new_name = body["name"]
    new_location = body["location"]
    current_user.name = new_name
    current_user.location = new_location
    current_user.save()
    return HttpResponse(status=200)


@csrf_exempt
def follow_user(request):
    if request.method != "POST":
        return HttpResponse(status=401)
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    current_user = body['current_user']
    followed_user = body['followed_user']
    try:
        checkAccessToken(request.headers.get("Authorization"), current_user['email'])
    except:
        return HttpResponse(status=401)
    c_user = User.objects.get(email=current_user['email'])
    f_user = User.objects.get(email=followed_user['email'])
    if not c_user.following.filter(email=f_user.email).exists():
        c_user.following.add(f_user)
        generate_event(f'User {c_user.name} followed {f_user.name}')
        return HttpResponse(content="User followed", status=200)
    return HttpResponse(content="User was already being followed", status=200)

@csrf_exempt
def get_following_list(request, email):
    if request.method != "GET":
        return HttpResponse(status=401)
    try:
        checkAccessToken(request.headers.get("Authorization"), email)
    except:
        return HttpResponse(status=401)
    user = User.objects.get(email=email)
    following_list = user.following.all()
    serializer = UserSerializer(following_list, many=True)
    for user in serializer.data:
        user['is_followed'] = 1
    return JsonResponse(serializer.data, status=200, safe=False)


@csrf_exempt
def get_search_list(request, email, search):
    try:
        checkAccessToken(request.headers.get("Authorization"), email)
    except:
        return HttpResponse(status=401)

    if request.method == 'GET':
        if search:
            queryset = User.objects.all().filter()
            queryset = queryset.filter(name__contains=search)
            if queryset:
                serializers = UserSerializer(queryset, many=True)
                current_user = User.objects.get(email=email)
                print(current_user)
                for user in serializers.data:
                    print(user['email'])
                    if current_user.following.filter(email=user['email']).exists():
                        user['is_followed'] = 1
                    else:
                        user['is_followed'] = 0
                return JsonResponse(serializers.data, status=200, safe=False)
        return JsonResponse(data={}, status=200)


def get_user(request, email):
    try:
        checkAccessToken(request.headers.get("Authorization"), email)
    except:
        return HttpResponse(status=401)
    user = User.objects.all().get(email=email)
    return JsonResponse(UserSerializer(user).data, status=200)





