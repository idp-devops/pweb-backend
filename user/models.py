from django.db import models


# Create your models here.
class User(models.Model):
    name = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    following = models.ManyToManyField("self", symmetrical=False)
    preferred_language = models.CharField(max_length=255, default="english")
    location = models.CharField(max_length=255, default="Kiev")

    def __str__(self):
        return f'{self.name} - {self.email}. Language: {self.preferred_language}, location: {self.location}'

    def getFollowersCity(self):
        list = []
        list.append(self.location)
        for follow in self.following.all():
            if not follow.location in list:
                list.append(follow.location)
        return list
