from rest_framework import serializers
from .models import User


class FollowerSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['name', 'email', 'location']


class UserSerializer(serializers.ModelSerializer):
    following = FollowerSerializer(many=True)

    class Meta:
        model = User
        fields = ['name', 'email', 'following', 'preferred_language', 'location']
