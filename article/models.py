from django.db import models
from user.models import User
# Create your models here.



class Article(models.Model):
    title = models.CharField(max_length=1000)
    description = models.CharField(max_length=5000)
    language = models.CharField(max_length=255)
    url = models.CharField(max_length=1000)
    image_url = models.CharField(max_length=1000, null=True)
    date = models.CharField(max_length=255)
    # following = models.ManyToManyField
    location = models.CharField(max_length=255, null=True, blank=True)