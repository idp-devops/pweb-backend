# Generated by Django 4.0.4 on 2022-05-20 17:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0006_remove_article_followed_by_article_followed_by'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='article',
            name='followed_by',
        ),
        migrations.AddField(
            model_name='article',
            name='location',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
