from django.urls import path
from . import views

urlpatterns = [
    path('get-news/<str:email>', views.get_articles),
    path('refresh/<str:email>', views.refresh_news)
]