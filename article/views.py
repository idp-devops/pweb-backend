from django.http import HttpResponse
from django.shortcuts import render
from user.models import User
# Create your views here.
from article.models import Article
from article.serializer import ArticleSerializer
from comment.models import Comment
from eventregistry import *
from firebase_auth.views import checkAccessToken

def add_article():
    new = Article(language="english", title="sdfe", description="rwfer", url="sfdvaerkh", date="ferg")
    new.save()
    serializer = ArticleSerializer(new)
    print(serializer.data)


def get_articles(request, email):
    try:
        checkAccessToken(request.headers.get("Authorization"), email)
    except:
        return HttpResponse(status=401)
    user = User.objects.get(email=email)
    locations = user.getFollowersCity()

    serializer = ArticleSerializer(Article.objects.filter(location__in=locations).order_by('-pk'), many=True)
    return HttpResponse(json.dumps(serializer.data), status=200)

def sync_articles(location):
    er = EventRegistry(apiKey='156d4515-7214-40ea-ab57-236cd565e8f5')
    qStr = """
    {
        "$query": {
            "$and": [
                {
                    "locationUri": "http://en.wikipedia.org/wiki/"
                },
                {
                    "lang": "eng"
                }
            ]
        },
        "$filter": {
            "forceMaxDataTimeWindow": "31",
            "dataType": [
                "news"
            ]
        }
    }
    """
    qStr = qStr.replace('http://en.wikipedia.org/wiki/', f'http://en.wikipedia.org/wiki/{location}')
    q = QueryArticlesIter.initWithComplexQuery(qStr)
    # change maxItems to get the number of results that you want
    for article in q.execQuery(er, maxItems=100):
        if not Article.objects.filter(title=article['title']).exists():
            try:
                Article.objects.create(
                    title=article["title"],
                    description=' '.join(article['body'].split()[:100]),
                    language=article['lang'],
                    url=article['url'],
                    date=article['dateTimePub'],
                    image_url=article['image'],
                    location=location
                )
            except Exception as e:
                print(e)


def call_for_all_users():
    users = User.objects.all()
    locations = []
    for user in users:
        if not user.location in locations:
            locations.append(user.location)
    for location in locations:
        sync_articles(location)


def refresh_news(request, email):
    try:
        checkAccessToken(request.headers.get("Authorization"), email)
    except:
        return HttpResponse(status=401)
    call_for_all_users()
    return HttpResponse(status=200)
