#!/bin/bash

echo "Sleeping 30, waiting for db to initialize"
sleep 30
echo "Starting server"
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py runserver 0.0.0.0:8000