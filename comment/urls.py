from django.urls import path
from . import views

urlpatterns = [
    path('add-comment', views.add_comment),
    path('<int:id>', views.get_comments)
]