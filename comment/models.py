from django.db import models


# Create your models here.
from article.models import Article
from user.models import User


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.CharField(max_length=3000)
    article = models.ForeignKey(Article, on_delete=models.CASCADE, default=1)