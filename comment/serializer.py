from rest_framework import serializers

from user.models import User

from .models import Comment


class MiniUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'name']


class CommentSerializer(serializers.ModelSerializer):
    user = MiniUserSerializer()

    class Meta:
        model = Comment
        fields = ['user', 'content', 'article']


