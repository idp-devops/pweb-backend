import json

from django.http import HttpResponse
# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from producer import generate_event
from article.models import Article
from comment.models import Comment
from comment.serializer import CommentSerializer
from user.models import User
from firebase_auth.views import checkAccessToken

@csrf_exempt
def add_comment(request):
    if request.method != "POST":
        return HttpResponse(status=401)
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    comment = body['comment']
    email = body['email']
    article_id = body['id']
    try:
        checkAccessToken(request.headers.get("Authorization"), email)
    except:
        return HttpResponse(status=401)
    current_user = User.objects.get(email=email)
    article = Article.objects.get(pk=article_id)
    new_comment = Comment(user=current_user, content=comment, article=article)
    new_comment.save()
    generate_event(f'[new_follow] User {current_user.name} just posted a comment: {comment}')
    return HttpResponse(status=200)


def get_comments(request, id):
    if request.method != "GET":
        return HttpResponse(status=401)
    article_comments = Comment.objects.filter(article__pk=id)
    serializer = CommentSerializer(article_comments, many=True)
    return HttpResponse(json.dumps(serializer.data), status=200)



