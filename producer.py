import pika


def generate_event(message):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.queue_declare(queue='queue')

    channel.basic_publish(
        exchange='',
        routing_key='queue',
        body=message
    )
    print(f" [x] Sent a message in queue: {message}")
    connection.close()

